%
% Define venue/journal string here and reuse in your bib entries for
% consistency. I include a list of many of the venues I often cite below.
% There are several reasons for doing this:
%
% Consistency: many online sources list the same venues slightly differently.
%
% History: Many journals/conferences have complicated histories. Some
% conferences change name (e.g., the rendering workshop turning into the
% rendering symposium), they also change the way they are published (e.g.,
% starting in 2008 EGSR is published in the journal Computer Graphics Forum).
%
% Define a string eliminates the need to remember the exact cutoffs and names.
%

% EGXR citations
% Since 2008, papers accepted at the Eurographics <Event> on Rendering are published as issue 4 of the journal Computer Graphics Forum.
@string{cgf_egsr08  = {Computer Graphics Forum (Proceedings of the Eurographics Symposium on Rendering)}}
% From 1995 to 2007, papers accepted at the Eurographics <Event> on Rendering were published in a book called Rendering Techniques <year>.
@string{egsr03-07   = {Rendering Techniques (Proceedings of the Eurographics Symposium on Rendering)} }
% The Eurographics Symposium on Rendering got its new Symposium status in 2003
@string{egwr95-02   = {Rendering Techniques (Proceedings of the Eurographics Workshop on Rendering)} }
% In 1994, papers accepted at the Eurographics Workshop on Rendering were published in a book called Photorealistic Rendering Techniques.
@string{egwr94      = {Photorealistic Rendering Techniques (Proceedings of the Eurographics Workshop on Rendering}}
% In 1992 proceedings were published in a book by Consolidation Express.
@string{egwr92      = {Third Eurographics Workshop on Rendering}}
% In 1991 the book was called Photorealistic Rendering in Computer Graphics, and in 1990 it was Photorealism in Computer Graphics.

% various other CGF publications
@string{cgf         = {Computer Graphics Forum}}
@string{cgf_eg      = {Computer Graphics Forum (Proceedings of Eurographics)}}
@string{cgf_pg      = {Computer Graphics Forum (Proceedings of Pacific Graphics)}}

% SIGGRAPH proceedings
% the first SIGGRAPH was not published in a separate journal
@string{siggraph74       = {Annual Conference Series (Proceedings of SIGGRAPH)}}
% SIGGRAPH between 1975 and 1992 (inclusive) were published in Computer Graphics
@string{siggraph75-92    = {Computer Graphics (Proceedings of SIGGRAPH)}}
% SIGGRAPH between 1993 and 2001 inclusive were not published elsewhere
@string{siggraph93-01    = {Annual Conference Series (Proceedings of SIGGRAPH)} }
% SIGGRAPH starting with 2002 are published in TOG
@string{tog_siggraph02   = {{ACM} Transactions on Graphics (Proceedings of SIGGRAPH)}}
@string{tog_siggraphasia = {{ACM} Transactions on Graphics (Proceedings of SIGGRAPH Asia)}}
@string{tog              = {{ACM} Transactions on Graphics}}

@string{gi          = {Proceedings of Graphics Interface}}
@string{hpg         = {Proceedings of High Performance Graphics}}
@string{vc_cgi      = {The Visual Computer (Proceedings of CGI)}}
@string{i3d         = {Proceedings of the Symposium on Interactive 3D Graphics and Games}}
@string{jgt         = {Journal of Graphics, GPU, and Game Tools}}
@string{tap         = {ACM TAP}}
@string{tvcg        = {IEEE TVCG}}
@string{irt         = {Proceedings of IEEE Symposium on Interactive Ray Tracing}}
@string{pg          = {Proceedings of Pacific Graphics}}
@string{vmv         = {Proceedings of Vision, Modeling and Visualization}}
@string{gafa        = {{GAFA} Geometric \& Functional Analysis}}
@string{cacm        = {Communications of the ACM}}

@string{ACM_COPY          = {© The Author(s) / ACM. This is the author's version of the work. It is posted here for your personal use. Not for redistribution. The definitive Version of Record is available at <a href="http://doi.acm.org">doi.acm.org</a>.}}
@string{EG_COPY           = {© The Author(s). This is the author's version of the work. It is posted here by permission of The Eurographics Association for your personal use. Not for redistribution. The definitive version is available at <a href="http://diglib.eg.org">diglib.eg.org</a>.}}

%
% end string defines
%



@article{singh17convergence,
	author = {Gurprit Singh and Wojciech Jarosz},
	title = {Convergence Analysis for Anisotropic Monte Carlo Sampling Spectra},
	journal = tog_siggraph02,
	volume = 36,
	number = 4,
	year = 2017,
	month = jul,
	doi = {10.1145/3072959.3073656},
	keywords = {stochastic sampling, signal processing, Fourier transform, Power spectrum},
	abstract = {Traditional Monte Carlo (MC) integration methods use point samples to numerically approximate the underlying integral. This approximation introduces variance in the integrated result, and this error can depend critically on the sampling patterns used during integration. Most of the well-known samplers used for MC integration in graphics---e.g.\ jittered, Latin-hypercube (N-rooks), multijittered---are anisotropic in nature. However, there are currently no tools available to analyze the impact of such anisotropic samplers on the variance convergence behavior of Monte Carlo integration. In this work, we develop a Fourier-domain mathematical tool to analyze the variance, and subsequently the convergence rate, of Monte Carlo integration using any arbitrary (anisotropic) sampling power spectrum. We also validate and leverage our theoretical analysis, demonstrating that judicious alignment of anisotropic sampling and integrand spectra can improve variance and convergence rates in MC rendering, and that similar improvements can apply to (anisotropic) deterministic samplers.},
	extra-affiliation-1 = {Dartmouth College},
	extra-author-affiliation-1 = {1},
	extra-author-affiliation-2 = {1},
	extra-caption = {The expected power spectrum of N-rooks with N=256 samples (left) is highly anisotropic,
with drastically different radial behavior along different directions (blue vs.\ red arrows). Fourier analysis using the radially averaged power spectrum (radial mean) cannot detect the good anisotropic properties of the sampler along the canonical axes.},
    extra-copyright = ACM_COPY
}

@article{bitterli17beyond,
	author = {Benedikt Bitterli and Wojciech Jarosz},
	title = {Beyond Points and Beams: Higher-Dimensional Photon Samples for Volumetric Light Transport},
	journal = tog_siggraph02,
	volume = 36,
	number = 4,
	year = 2017,
	month = jul,
	doi = {10.1145/3072959.3073698},
	keywords = {track-length estimator, expected value estimator, photon beams, photon mapping, participating media},
	abstract = {We develop a theory of volumetric density estimation which generalizes prior photon point (0D) and beam (1D) approaches to a broader class of estimators using ``nD'' samples along photon and/or camera subpaths. Volumetric photon mapping performs density estimation by point sampling propagation distances within the medium and performing density estimation over the generated points (0D). Beam-based (1D) approaches consider the expected value of this distance sampling process along the last camera and/or light subpath segments. Our theory shows how to replace propagation distance sampling steps across <i>multiple bounces</i> to form higher-dimensional samples such as photon planes (2D), photon volumes (3D), their camera path equivalents, and beyond. We perform a theoretical error analysis which reveals that in scenarios where beams already outperform points, each additional dimension of nD samples compounds these benefits further. Moreover, each additional sample dimension reduces the required dimensionality of the blurring needed for density estimation, allowing us to formulate, for the first time, <i>fully unbiased</i> forms of volumetric photon mapping. We demonstrate practical implementations of several of the new estimators our theory predicts, including both biased and unbiased variants, and show that they outperform state-of-the-art beam-based volumetric photon mapping by a factor of 2.4--40×.},
	extra-affiliation-1 = {Dartmouth College},
	extra-author-affiliation-1 = {1},
	extra-author-affiliation-2 = {1},
	extra-caption = {We generalize 0D photon points (a) and 1D beams (b) to produce progressively higher-dimensional nD samples such as 2D ``photon planes'' (c) and 3D ``photon volumes'' (d). We form these estimators by computing the limit process of ``marching'' or sweeping photons along preceding light path segments, which allows us to progressively reduce variance and bias. The motivational experiment in the bottom row uses these successive estimators (each shown vertically split at two sample counts) on a searchlight problem setup (left), confirming that higher-order nD samples have the potential to dramatically improve quality in volumetric light transport.},
	extra-data-download-1 = {<a href="bitterli17beyond-supplemental.zip">Supplemental archive, ZIP (125MB)</a> - additional results and comparisons},
	extra-code-download-1 = {<a href="https://benedikt-bitterli.me/photon-planes/webgl/">WebGL demo of our method (external link)</a>},
	extra-code-download-2 = {Full reference implementation (coming soon!)},
    extra-copyright = ACM_COPY
}


@patent{jarosz2013method-patent,
	author = {Wojciech Jarosz and Nathan A. Carr},
	title = {Method and apparatus for converting spherical harmonics representations of functions into multi-resolution representations},
	year = 2017,
	month = jul,
	day = 11,
	number = {9703756 B2},
	type = {Patent},
	location = {US},
	url = {https://www.google.com/patents/US9703756},
	yearfiled = 2009,
	monthfiled = jan,
	dayfiled = 15,
}
